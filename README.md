## SedaQ OIDC Issuer over HTTP

Maven and Java SDK must be installed to build the project.

```bash
apt get install openjdk-11-jdk maven
```

## Docker

### Build and Run OIDC Issuer
Docker build.

```bash
docker build -t sedaq-oidc-issuer .
```

Docker run.
```bash
docker run -p 8080:8080 -it -v /c/Gopas/JavaDays/2019/workspace/db/hsqldb:/c/Gopas/JavaDays/2019/workspace/db/hsqldb --name sedaq-oidc-issuer-container sedaq-oidc-issuer
```