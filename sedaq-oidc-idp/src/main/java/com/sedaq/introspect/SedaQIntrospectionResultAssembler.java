package com.sedaq.introspect;

import com.google.common.collect.Sets;
import com.google.gson.JsonElement;
import com.google.gson.JsonPrimitive;
import org.mitre.oauth2.model.OAuth2AccessTokenEntity;
import org.mitre.oauth2.service.impl.DefaultIntrospectionResultAssembler;
import org.mitre.openid.connect.config.ConfigurationPropertiesBean;
import org.mitre.openid.connect.model.UserInfo;
import org.mitre.openid.connect.service.ScopeClaimTranslationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Assembler of result obtained from introspection endpoint.
 * Adds "iss" to identify issuer in response from Introspection endpoint to Resource Server.
 *
 * @author Pavel Seda
 */
@Service
@Primary
public class SedaQIntrospectionResultAssembler extends DefaultIntrospectionResultAssembler {

    private final static Logger LOG = LoggerFactory.getLogger(SedaQIntrospectionResultAssembler.class);

    private ConfigurationPropertiesBean configBean;
    private ScopeClaimTranslationService translator;

    /**
     * Instantiates a new Kypo introspection result assembler.
     *
     * @param configBean the config bean
     * @param translator the translator
     */
    @Autowired
    public SedaQIntrospectionResultAssembler(ConfigurationPropertiesBean configBean, ScopeClaimTranslationService translator) {
        this.configBean = configBean;
        this.translator = translator;
    }

    @Override
    public Map<String, Object> assembleFrom(OAuth2AccessTokenEntity accessToken, UserInfo userInfo, Set<String> resourceServerScopes) {
        Map<String, Object> map = super.assembleFrom(accessToken, userInfo, resourceServerScopes);
        Set<String> accessTokenScopes = accessToken.getScope();
        Set<String> scopes = Sets.intersection(resourceServerScopes, accessTokenScopes);
        Map<String, JsonElement> additionalClaims = getAdditionalClaims(userInfo, scopes);
        map.putAll(additionalClaims);
        LOG.debug("Claims added into introspection result: {}", additionalClaims);
        return map;
    }

    private Map<String, JsonElement> getAdditionalClaims(UserInfo userInfo, Set<String> scopes) {
        Set<String> authorizedClaims = translator.getClaimsForScopeSet(scopes);
        Map<String, JsonElement> result = userInfo.toJson()
                .entrySet()
                .stream()
                .filter(claim -> authorizedClaims.contains(claim.getKey()) && claim.getValue() != null && !claim.getValue().isJsonNull())
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));

        result.put("iss", new JsonPrimitive(configBean.getIssuer()));
        return result;
    }

}