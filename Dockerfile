FROM maven:3.6.2-jdk-11-slim AS plr
LABEL maintaner="Pavel Seda"
COPY ./ /apps
WORKDIR /apps
RUN mvn clean package -DskipTests

FROM openjdk:11-jdk AS jdk
COPY --from=pkg /apps/sedaq-oidc-idp/etc/ /apps/etc/
COPY --from=plr /apps/sedaq-oidc-idp/jetty-docker.xml /apps/jetty.xml
COPY --from=plr /apps/sedaq-oidc-idp/target/sedaq-oidc.war /apps/sedaq-oidc.war
WORKDIR /apps
ADD https://repo1.maven.org/maven2/org/eclipse/jetty/jetty-runner/9.4.19.v20190610/jetty-runner-9.4.19.v20190610.jar /apps/jetty-runner-9.jar

CMD ["java", "-jar",  "jetty-runner-9.jar", "--config", "jetty.xml", "--path", "/sedaq-oidc", "sedaq-oidc.war"]
EXPOSE 8080